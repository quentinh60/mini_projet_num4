from tkinter import *
import datetime

master = Tk()

canvas = Canvas(master, width = 700, height = 250)
canvas.pack()

element_id = canvas.create_oval(10, 20, 30, 40, fill='blue')

def click_callback(event):
    x1 = event.x
    y1 = event.y
    print(x1)
    print(y1)
    color = 'blue'
    if event.num == 1:
        color = 'red'
    elif event.num ==3:
        color = 'yellow'
    canvas.create_rectangle(x1, y1, x1 +20, y1 +20, fill=color)

canvas.bind('<Button-1>', click_callback)
canvas.bind('<Button-1>', click_callback)
canvas.bind('<Button-2>', click_callback)
canvas.bind('<Button-3>', click_callback)
canvas.create_line(150, 80, 200, 100, fill='blue', width = 3)

master.mainloop()