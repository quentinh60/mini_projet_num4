from tkinter import *
master = Tk()

canvas_width = 80
canvas_height = 40

canvas = Canvas(master, width = canvas_width, height = canvas_height)

canvas.pack()

y = canvas_height / 2
canvas.create_line(0, y, canvas_width, y, fill="#476042")

master.mainloop()