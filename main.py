from tkinter import *
import random
import sys
import time
import argparse
import datetime


def initialisation():
    tab = []
    for i in range(0, rows,  1):
        tableau = []
        for j in range(0, cols , 1):
            x1 = j * cell_size
            y1 = i * cell_size
            x2 = x1 + cell_size
            y2 = y1 + cell_size
            pourcentage = random.random()
            if pourcentage <= afforestation:
                tableau.append(canvas.create_rectangle(x1, y1, x2, y2, fill='green'))
            else:
                tableau.append(canvas.create_rectangle(x1, y1, x2, y2, fill="white"))
            y1 += cell_size
            y2 += cell_size
        tab.append(tableau)
    return tab
    

def changement_etats(a_mettre_en_feu, a_mettre_en_cendre, a_mettre_en_vide ):
    for i in range(len(a_mettre_en_feu)):
        canvas.itemconfig(a_mettre_en_feu[i], fill = "red")
    for i in range(len(a_mettre_en_cendre)):
        canvas.itemconfig(a_mettre_en_cendre[i], fill = "grey")
    for i in range(len(a_mettre_en_vide)):
        canvas.itemconfig(a_mettre_en_vide[i], fill = "white")
    

def animation():
    continuer = False
    a_mettre_en_feu = []
    a_mettre_en_cendre = []
    a_mettre_en_vide = []
    for i in range(rows):
        for j in range(cols):
            if regle.get() == 1:
                if canvas.itemcget(tab_cellules[i][j], "fill") == "red" :
                    continuer = True
                    if i!=0:
                        if canvas.itemcget(tab_cellules[i-1][j], "fill") == "green":
                            a_mettre_en_feu.append(tab_cellules[i-1][j])
                    if i!= (rows-1):
                        if canvas.itemcget(tab_cellules[i+1][j], "fill") == "green":
                            a_mettre_en_feu.append(tab_cellules[i+1][j])
                    if j!=0:
                        if canvas.itemcget(tab_cellules[i][j-1], "fill") == "green":
                            a_mettre_en_feu.append(tab_cellules[i][j-1])
                    if j!=(cols-1):
                        if canvas.itemcget(tab_cellules[i][j+1], "fill") == "green":
                            a_mettre_en_feu.append(tab_cellules[i][j+1]) 
                    a_mettre_en_cendre.append(tab_cellules[i][j])
            else:
                k = 0
                if canvas.itemcget(tab_cellules[i][j], "fill") == "green" :
                    if i!=0:
                        if canvas.itemcget(tab_cellules[i-1][j], "fill") == "red":
                            k+=1
                    if i!= (rows-1):
                        if canvas.itemcget(tab_cellules[i+1][j], "fill") == "red":
                            k+=1
                    if j!=0:
                        if canvas.itemcget(tab_cellules[i][j-1], "fill") == "red":
                            k+=1
                    if j!=(cols-1):
                        if canvas.itemcget(tab_cellules[i][j+1], "fill") == "red":
                            k+=1 
                    if random.random()<(1-1/(k+1)):
                        a_mettre_en_feu.append(tab_cellules[i][j]) 
                        continuer = True
                if canvas.itemcget(tab_cellules[i][j], "fill") == "red" :
                    continuer = True
                    a_mettre_en_cendre.append(tab_cellules[i][j])
            if canvas.itemcget(tab_cellules[i][j], "fill") == "grey" :
                continuer = True
                a_mettre_en_vide.append(tab_cellules[i][j])
    changement_etats(a_mettre_en_feu, a_mettre_en_cendre, a_mettre_en_vide )

    if continuer == True:
        master.after(int(animation_time*1000), animation)


def click_callback(event):
    cellule = canvas.find_closest(event.x, event.y)
    if canvas.itemcget(cellule, "fill") == "green":
        canvas.itemconfigure(cellule, fill='red')

def main():
    boutonfonc = canvas.bind('<Button-1>', click_callback)
    master.mainloop()


parser = argparse.ArgumentParser()
parser.add_argument("-rows", help="set number of rows")
parser.add_argument("-cols", help="set number of cols")
parser.add_argument("-cell_size", help="set cell's size")
parser.add_argument("--afforestation", help="set simulation's afforestation")
parser.add_argument("--animation", help="set animation delay")

rows = 10
cols = 20
cell_size = 35
animation_time = 0.2
afforestation = 0.6

args = parser.parse_args()
if args.rows :
    rows = int(args.rows)
if args.cols :
    cols = int(args.cols)
if args.cell_size :
    cell_size = int(args.cell_size)
if args.animation :
    animation_time = float(args.animation)
if args.afforestation :
    afforestation = float(args.afforestation)

master = Tk()

master.rowconfigure(0, weight=1)
master.rowconfigure(1, weight=1)
master.rowconfigure(2, weight=1)
master.columnconfigure(0, weight=1)
master.columnconfigure(1, weight=1)

canvas = Canvas(master, width = cols*cell_size, height = rows*cell_size)
Start = Button(master, text = "Simulation", command = animation, background = "lightgrey")
regle = IntVar()
regle.set(1)
regle1 = Radiobutton(master, text = "règle 1", variable = regle, value = 1 )
regle2 = Radiobutton(master, text = "règle 2", variable = regle, value = 2 )
canvas.grid(row = 0, columnspan = 2)
regle1.grid(row = 1, column = 1)
regle2.grid(row = 2, column = 1)
Start.grid(row = 1, rowspan = 2, column = 0, padx = 15, pady = 20)

tab_cellules = initialisation()


if __name__ == '__main__':
    main()
